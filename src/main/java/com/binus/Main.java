package com.binus;

public class Main {

    public static void drawRightLeftUpwardTriangle(){
        String star="";
        for (int a=1;a<=3;a++){
            star+="*";
            System.out.println(star);
        }
    }

    public static void drawRightRightDownwardTriangle(){
        for (int a=0;a<3;a++){
            for(int b=0;b<3;b++){
                if(b<a) System.out.print(" ");
                else System.out.print("*");
            }
            System.out.println("");
        }
    }

    public static void main(String[] args) {
        drawRightLeftUpwardTriangle();
        System.out.println(" ");

        drawRightRightDownwardTriangle();
    }
}
